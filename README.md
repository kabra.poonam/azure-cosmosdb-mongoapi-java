## Instructions
* Prereq.
    * Installing JDK 1.7+ and Maven (apt-get install default-jdk maven)
* Clone the code
    * git clone https://gitlab.com/ubs-workshop-batch-5/azure-cosmosdb-mongoapi-java
    * cd azure-cosmosdb-mongoapi-java
* Update the connection string in the Program.java file which is there in src directory
    * vi src/GetStarted/Program.java
* Compile the code and resolve dependinces
    * mvn compile
* Execute the code
    * mvn exec:java -D exec.mainClass=GetStarted.Program